Django==2.1
gunicorn==19.6.0
psycopg2==2.6.2
djangorestframework==3.8.2
coverage==4.5.1
coreapi==2.3.3
Markdown==2.6.11
Pygments==2.2.0