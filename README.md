
Meeting Room Reserver
=======
*Meeting Room Reserver* is a simple REST API made with [Django Rest Framework](http://www.django-rest-framework.org/) where you can create rooms, schedule events and search them.


How to run this project
----
 To run this project, first you need to clone last version of the repo at the URL: https://philliparente@bitbucket.org/philliparente/meeting_room_reserver.git

    $ git clone https://philliparente@bitbucket.org/philliparente/meeting_room_reserver.git
---
Then, you need to enter into repo directory:

    $ cd meeting_room_reserver/
    
you can set the project environment for "prod" mode (This will disable DEBUG flag in django):

    $ export PROJECT_ENVIRONMENT=prod


And then, up the local docker compose context ([Docker compose required](https://docs.docker.com/compose/)):

    $ docker-compose up
    
After all docker compose stuff, the project will be available at [http://localhost:8000](http://localhost:8000) 

*(Django could fail its first try to connect to database container, but it will restart and connect successfully in few seconds automatically)*

**NOTE:** 
Each django config, like data base settings, logs e etc can be overridden by a local_settings.py file placed into meeting_room_reserver directory (meeting_room_reserver/local_settings.py) 


**Code coverage**

The Code Coverage at the moment is: **89%** and it is detailed at [coverage/](http://localhost:8000/coverage/) after Testing section commands, and "coverage html"


Making some requests
----
The full api documentation can be found at [api/docs](http://localhost:8000/api/docs)

**NOTE:** All requests MUST to be prefixed by a version path parameter (api/v{version}/something). The only and the last version, at now, is 1.0, so all endpoints will be available at *"api/v1.0/something/"*

Here is some **examples**:

Creating a Room (POST request with JSON data):

    $ curl -H "Content-Type: application/json" -d '{"name": "My meeting Room"}' -X POST http://localhost:8000/api/v1.0/rooms/

Searching Rooms (GET request with query parameters):

    $ curl -H "Content-Type: application/json" -X GET http://localhost:8000/api/v1.0/rooms/?name=meeting

Updating Rooms (PATCH request with JSON DATA and path parameter "Room id"):

    $ curl -H "Content-Type: application/json" -d '{"name": "My another meeting room name"}' -X PATCH http://localhost:8000/api/v1.0/rooms/1
    
       
   
Schedule Events (POST request with JSON data):

    $ curl -H "Content-Type: application/json" -d '{ "title": "My meeting", "room": 1, "from_date": "2018-08-20T21:00Z", "to_date": "2018-08-20T22:00Z" }' -X POST http://localhost:8000/api/v1.0/schedulings/

Deleting Events (DELETE request with the path parameter "Scheduling id"):

    $ curl -H "Content-Type: application/json"  -X DELETE http://localhost:8000/api/v1.0/schedulings/1
---

**Some others endpoints docs can be found at [api/docs](http://localhost:8000/api/docs)**


## Testing
To get the code coverage, you can make it by entering into django container (dg01):

    $ docker exec -it dg01 bash
And then, run:

    $ coverage run --source="." manage.py test
To check the coverage *stdout* report, run:

    $ coverage report
To check the HTML coverage report version, run:

    $ coverage html
 In the HTML case, it is possible to access the full detailed coverage at [coverage/](http://localhost:8000)

## Logging
Each request and response will generate a detailed log, with the request and response attributes inside the root project directory "logs/".

**Request format**

    LEVEL:{levelname} ASCTIME:{asctime} PATH:{PATH} METHOD:{METHOD} BODY:{BODY}

*Example:*

        LEVEL:INFO ASCTIME:2018-08-20 22:04:00,136 PATH:/api/v1.0/schedulings/ METHOD:POST BODY:b'{"from_date":"2018-08-17T13:30","room":12,"title":"New Meeting3","to_date":"2018-08-17T14:30"}'

**Response format**

    LEVEL:{levelname} ASCTIME:{asctime} STATUS:{HTTP_STATUS} CONTENT:{CONTENT}
*Example:*

    LEVEL:INFO ASCTIME:2018-08-20 22:21:58,361 STATUS:201 CONTENT:b'{"id":5,"title":"New Meeting","room":12,"from_date":"2018-08-17T14:00:00Z","to_date":"2018-08-17T15:00:00Z"}'
