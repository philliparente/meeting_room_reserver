import logging

request_logger = logging.getLogger('request')
response_logger = logging.getLogger('response')

class LogMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response



    def __call__(self, request):

        request_logger.info("PATH:{} METHOD:{} BODY:{}".format(request.path, request.method, request.body))
        response = self.get_response(request)
        response_logger.info("STATUS:{} CONTENT:{}".format(response.status_code, response.content))

        return response