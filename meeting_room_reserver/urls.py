"""meeting_room_reserver URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic import RedirectView
from rest_framework.documentation import include_docs_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls')),

    url(r'^api/v(?P<version>(1.0))/', include('apps.rest_api.urls', namespace='api')),

    url(r'^api/docs/?', include_docs_urls(title='API Documentation', public=True)),
    url(r'^.*$', RedirectView.as_view(pattern_name='api-docs:docs-index', permanent=True))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

