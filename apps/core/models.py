from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext as _
# Create your models here.


class Room(models.Model):
    name = models.CharField(max_length=255, verbose_name=_("Name"), help_text=_("The schedulings's title"))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("Room")
        verbose_name_plural = _("Rooms")
        ordering = ['id']




class Scheduling(models.Model):

    title_help_text = _("The schedulings's title")
    room_help_text = _("The room id where the scheduling is scheduled")
    from_date_help_text = _(
        "The Date time when the scheduling starts in ISO 8601 format (YYYY-MM-DDThh:mm:ssTZD). Ex: (2018-08-17T14:00:00Z)")
    to_date_help_text = _(
        "The Date time when the scheduling ends in ISO 8601 format (YYYY-MM-DDThh:mm:ssTZD). Ex: (2018-08-17T14:00:00Z)")

    title = models.CharField(max_length=255, verbose_name=_("Title"), help_text=title_help_text)
    room = models.ForeignKey(Room, on_delete=models.CASCADE, verbose_name=_("Room"), related_name=_("schedulings"), help_text=room_help_text)


    from_date = models.DateTimeField(verbose_name=_("From date"), help_text=from_date_help_text)
    to_date = models.DateTimeField(verbose_name=_("To date"), help_text=to_date_help_text)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['id']
        verbose_name = _("Scheduling")
        verbose_name_plural = _("Schedulings")