from django.contrib import admin

# Register your models here.
from apps.core.models import Room, Scheduling

admin.site.register(Room)
admin.site.register(Scheduling)