from django.db.models import Q
from rest_framework import serializers
from django.utils.translation import gettext as _
from apps.core.models import Room, Scheduling
from apps.rest_api.utils import SearchSerializer


class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = (
            "id",
            'name',

        )
        read_only_fields = ["id"]


class SchedulingSerializer(serializers.ModelSerializer):
    def validate(self, attrs):

        from_date = attrs['from_date']
        to_date = attrs['to_date']
        room = attrs['room']

        if from_date > to_date:
            raise serializers.ValidationError("From Date cannot be greater than To Date", code='invalid')

        if Scheduling.objects.filter(
                        Q(room=room) &
                        ((Q(from_date__gte=from_date) & Q(from_date__lt=to_date))
                             | Q(to_date__gt=from_date, to_date__lte=to_date))
        ).exists():
            raise serializers.ValidationError("The selected room is not available", code='invalid')

        return super(SchedulingSerializer, self).validate(attrs=attrs)

    class Meta:
        model = Scheduling
        fields = (
            "id",
            'title',
            'room',
            'from_date',
            'to_date',

        )
        read_only_fields = ["id"]


class SearchRoomSerializer(SearchSerializer):
    def get_queryset(self):
        return Room.objects.all()

    name = serializers.CharField(max_length=255, required=False, help_text=_("Substring into name attribute"))

    # Overriding to customize queryset
    def filter(self, raise_exception=True):
        self.is_valid(raise_exception=raise_exception)
        queryset = self.get_queryset()
        if 'name' in self.data:
            queryset = queryset.filter(name__icontains=self.data['name'])
        return queryset

class SearchSchedulingSerializer(SearchSerializer):
    def get_queryset(self):
        return Scheduling.objects.all()

    title = serializers.CharField(max_length=255, required=False, help_text=_("Substring into title attribute"))
    room_id = serializers.IntegerField(required=False, help_text=_("The room's id where the scheduling is scheduled"))
    from_date = serializers.DateTimeField(required=False, help_text=Scheduling.from_date_help_text)
    to_date = serializers.DateTimeField(required=False, help_text=Scheduling.to_date_help_text)

    # Overriding to customize queryset
    def filter(self, raise_exception=True):
        self.is_valid(raise_exception=raise_exception)
        queryset = self.get_queryset()
        if 'title' in self.data:
            queryset = queryset.filter(title__icontains=self.data['title'])

        if 'room_id' in self.data:
            queryset = queryset.filter(room__id=self.data['room_id'])

        if 'from_date' in self.data:
            queryset = queryset.filter(from_date=self.data['from_date'])

        if 'to_date' in self.data:
            queryset = queryset.filter(to_date=self.data['to_date'])

        return queryset
