from django.conf.urls import url
from apps.rest_api import views

app_name = 'rest_api'
urlpatterns = [
    # ROOM
    url(r"^rooms/$", views.RoomViewset.as_view({
        "get": "list",
        "post": "create"
    }), name='room-simple'),
    url(r"^rooms/(?P<pk>\d+)/?$", views.RoomViewset.as_view(
        {
            "get":  "retrieve",
            "patch":  "partial_update",
            "delete":  "destroy",
        }
    ), name="room-pk"),

    # SCHEDULING
    url(r"^schedulings/$", views.SchedulingViewset.as_view({
        "get": "list",
        "post": "create"
    }), name='scheduling-simple'),
    url(r"^schedulings/(?P<pk>\d+)/?$", views.SchedulingViewset.as_view(
        {
            "get":  "retrieve",
            "patch":  "partial_update",
            "delete":  "destroy",
        }
    ), name="scheduling-pk"),


]
