from apps.core.models import Room, Scheduling
from rest_framework import status
from django.db import models

from apps.rest_api.test_base import AbstractTestCase, BaseTestClient


class RoomAPITestCase(AbstractTestCase):

    def get_test_list_data(self) -> dict:
        return {'name': 'New Test List Room'}

    API_VERSION = "1.0"
    abstract = False


    def get_test_patch_after_data(self) -> dict:
        return {'name': 'New After Room'}

    def get_client(self) -> BaseTestClient:
        return BaseTestClient("api:room-simple", "api:room-pk", RoomAPITestCase.API_VERSION)

    def get_test_post_data(self) -> dict:
        return {'name': 'New Room'}

    def get_test_patch_before_data(self) -> dict:
        return {'name': 'New Before Room'}

    def get_model(self) -> models.Model:
        return Room

    def get_test_delete_data(self) -> dict:
        return {'name': 'New Room'}


class SchedulingAPITestCase(AbstractTestCase):



    API_VERSION = "1.0"
    abstract = False

    def get_test_list_data(self) -> dict:
        room = Room.objects.create(name='New room')
        return {'title': 'New Meeting', 'room_id': room.id, 'from_date': "2018-08-17T13:05", 'to_date': "2018-08-17T14:05"}

    def get_test_patch_after_data(self) -> dict:
        room = Room.objects.create(name='New room')

        return {'title': 'New Meeting', 'room': room.id, 'from_date': "2018-08-17T13:05", 'to_date': "2018-08-17T14:05"}

    def get_client(self) -> BaseTestClient:
        return BaseTestClient("api:scheduling-simple", "api:scheduling-pk", SchedulingAPITestCase.API_VERSION)

    def get_test_post_data(self) -> dict:
        room = Room.objects.create(name='New room')
        return {'title': 'New Meeting', 'room': room.id, 'from_date': "2018-08-17T13:05", 'to_date': "2018-08-17T14:05"}

    def get_test_patch_before_data(self) -> dict:
        room = Room.objects.create(name='New room')
        return {'title': 'New Meeting', 'room': room, 'from_date': "2018-08-17T13:05", 'to_date': "2018-08-17T14:05"}

    def get_model(self) -> models.Model:
        return Scheduling

    def get_test_delete_data(self) -> dict:
        room = Room.objects.create(name='New room')
        return {'title': 'New Meeting', 'room': room, 'from_date': "2018-08-17T13:05", 'to_date': "2018-08-17T14:05"}


    def test_post(self):
        # Test data
        test_data = self.get_test_post_data()

        # Creating with a POST request
        response = self.client.post(data=self.get_test_post_data(), format='json')

        # Testing success HTTP status
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_patch(self):

        test_data = self.get_test_patch_before_data()

        # Creating new instance
        instance = self.get_model().objects.create(**test_data)

        # Another data to test after
        new_test_data = self.get_test_patch_after_data()

        # Partial update with a PATCH request
        response = self.client.patch(instance.id, data=new_test_data, format='json')

        # Testing success HTTP status
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_scheduling_with_unavailable_room(self):
        room = Room.objects.create(name='New room')
        test_data = {
            'title': 'New Meeting', 'room': room.id, 'from_date': "2018-08-17T14:00", 'to_date': "2018-08-17T15:00"
        }

        another_test_data = {
            'title': 'New Meeting2', 'room': room.id, 'from_date': "2018-08-17T14:05", 'to_date': "2018-08-17T15:00"
        }

        third_test_data = {
            'title': 'New Meeting3', 'room': room.id, 'from_date': "2018-08-17T14:30", 'to_date': "2018-08-17T16:00"
        }
        fourth_test_data = {
            'title': 'New Meeting3', 'room': room.id, 'from_date': "2018-08-17T13:30", 'to_date': "2018-08-17T14:30"
        }

        first_response = self.client.post(data=test_data, format='json')
        second_response = self.client.post(data=another_test_data, format='json')
        third_response = self.client.post(data=third_test_data, format='json')
        fourth_response = self.client.post(data=fourth_test_data, format='json')

        self.assertEqual(first_response.status_code, status.HTTP_201_CREATED)

        self.assertNotEqual(second_response.status_code, status.HTTP_201_CREATED, msg="Conflict in second response")
        self.assertNotEqual(third_response.status_code, status.HTTP_201_CREATED, msg="Conflict in third response")
        self.assertNotEqual(fourth_response.status_code, status.HTTP_201_CREATED, msg="Conflict in fourth response")


    def test_scheduling_to_date_greater_than_from_date(self):
        room = Room.objects.create(name='New room')
        test_data = {
            'title': 'New Meeting', 'room': room.id, 'from_date': "2018-08-17T15:00", 'to_date': "2018-08-17T14:00"
        }

        response = self.client.post(data=test_data, format='json')

        if response.status_code == status.HTTP_201_CREATED:
            self.fail("To date is not greater than From Date")

