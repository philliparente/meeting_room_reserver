import coreapi
import coreschema
from rest_framework import serializers
from rest_framework.schemas import AutoSchema
from django.utils.translation import gettext as _
import re
from django.conf import settings

class SearchSerializer(serializers.Serializer):
    def __init__(self, data=None):
        super(SearchSerializer, self).__init__(instance=None, data=data)

    def get_queryset(self):
        raise NotImplementedError('`get_queryset()` must be implemented.')

    def update(self, instance, validated_data):
        # Not required
        pass

    def create(self, validated_data):
        # Not required
        pass

    def filter(self, raise_exception=True):
        self.is_valid(raise_exception=raise_exception)
        return self.get_queryset().filter(**self.data)


class CustomAutoSchema(AutoSchema):

    regex_read = re.compile(r'\/.+\/\{id\}\/')

    def __init__(self, search_serializer: SearchSerializer, manual_fields=None):
        self.search_serializer = search_serializer

        super(CustomAutoSchema, self).__init__(manual_fields=manual_fields)

    def get_manual_fields(self, path, method):

        manual_fields = super(CustomAutoSchema, self).get_manual_fields(path=path, method=method)
        if manual_fields is None:
            manual_fields = []
        extra_fields = [
            coreapi.Field(
                "version",
                required=True,
                location="path",
                schema=coreschema.String(description=_("The API's version"), default=settings.LAST_API_VERSION)
            )
        ]
        match = self.regex_read.match(path)

        if method == 'GET' and not match:

            for field_name, field in self.search_serializer.get_fields().items():
                extra_fields.append(coreapi.Field(
                    field_name,
                    required=field.required,
                    location="query",
                    schema=coreschema.String(description=field.help_text, max_length=255)
                ))
        return manual_fields + extra_fields

    def get_link(self, path, method, base_url):

        return super(CustomAutoSchema, self).get_link(path=path, method=method, base_url=settings.SITE_URL)
