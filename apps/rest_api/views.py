import coreapi
import coreschema
from rest_framework import viewsets
from rest_framework.schemas import AutoSchema
from rest_framework.schemas import ManualSchema

from apps.core.models import Room, Scheduling
from apps.rest_api.pagination import DefaultResultsSetPagination
from apps.rest_api.serializers import RoomSerializer, SchedulingSerializer, SearchSchedulingSerializer, \
    SearchRoomSerializer
from apps.rest_api.utils import CustomAutoSchema



class RoomViewset(viewsets.ModelViewSet):
    serializer_class = RoomSerializer
    pagination_class = DefaultResultsSetPagination
    search_serializer = SearchRoomSerializer

    schema = CustomAutoSchema(search_serializer())

    queryset = Room.objects.all()

    def get_queryset(self):
        search_serializer = self.search_serializer(data=self.request.query_params)

        return search_serializer.filter()



class SchedulingViewset(viewsets.ModelViewSet):
    serializer_class = SchedulingSerializer
    pagination_class = DefaultResultsSetPagination
    search_serializer = SearchSchedulingSerializer
    schema = CustomAutoSchema(search_serializer())
    queryset = Scheduling.objects.all()

    def get_queryset(self):

        search_serializer = self.search_serializer(data=self.request.query_params)


        return search_serializer.filter()
