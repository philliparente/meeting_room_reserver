from django.forms import model_to_dict
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status
from django.db import models
from django.test import TestCase
from django.utils.http import urlencode

class BaseTestClient(APIClient):
    API_PREFIX = 'api/'


    @property
    def API_SUFFIX(self):
        raise NotImplementedError

    def __init__(self, simple_namespace, namespace_with_pk, version, enforce_csrf_checks=False, **defaults):
        self.version = version
        self.namespace_with_pk = namespace_with_pk
        self.reversed_simple = reverse(simple_namespace, kwargs={"version": self.version})

        super(BaseTestClient, self).__init__(enforce_csrf_checks, **defaults)

    def list(self, query_params=None, follow=False, **extra):
        path = '{}?{}'.format(self.reversed_simple, urlencode(query_params))
        return super(BaseTestClient, self).get(path, data=None, follow=follow, **extra)

    def post(self, data=None, format=None, content_type=None, follow=False, **extra):

        return super(BaseTestClient, self).post(self.reversed_simple, data=data, format=format, content_type=content_type, follow=follow, **extra)

    def delete(self, id, data=None, format=None, content_type=None, follow=False, **extra):
        path = reverse(self.namespace_with_pk, kwargs={"version": self.version, "pk": id})
        return super(BaseTestClient, self).delete(path, data=data, format=format, content_type=content_type, follow=follow, **extra)

    def patch(self, id, data=None, format=None, content_type=None, follow=False, **extra):
        path = reverse(self.namespace_with_pk, kwargs={"version": self.version, "pk": id})
        return super(BaseTestClient, self).patch(path, data=data, format=format, content_type=content_type, follow=follow, **extra)

    def put(self, id, data=None, format=None, content_type=None, follow=False, **extra):
        path = reverse(self.namespace_with_pk, kwargs={"version": self.version, "pk": id})
        return super(BaseTestClient, self).put(path, data=data, format=format, content_type=content_type, follow=follow, **extra)

    def get(self, id, data=None, format=None, content_type=None, follow=False, **extra):
        path = reverse(self.namespace_with_pk, kwargs={"version": self.version, "pk": id})
        return super(BaseTestClient, self).get(path, data=data, format=format, content_type=content_type, follow=follow, **extra)





# Creating a base test case class for DRY, but deep validating test are overrided
class AbstractTestCase(TestCase):

    abstract = True

    def run(self, result=None):
        # to avoid test runner on an abstract class
        if not self.abstract:
            return super(AbstractTestCase, self).run(result=result)

    # Subclasses must provide how to get client
    def get_client(self) -> BaseTestClient:
        raise NotImplementedError

    # Subclasses must provide the request data

    def get_test_post_data(self) -> dict:
        raise NotImplementedError

    def get_test_patch_before_data(self) -> dict:
        raise NotImplementedError

    def get_test_patch_after_data(self) -> dict:
        raise NotImplementedError

    def get_test_delete_data(self) -> dict:
        raise NotImplementedError

    def get_test_list_data(self) -> dict:
        raise NotImplementedError

    def setUp(self):
        self.client = self.get_client()

    def get_model(self) -> models.Model:
        raise NotImplementedError

    def test_list(self):
        # Test data
        test_data = self.get_test_list_data()

        #Create an instance
        instance = self.get_model().objects.create(**test_data)

        #search instance by params
        response = self.client.list(query_params=test_data, format='json')

        # Assert status
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Assert t
        self.assertGreater(len(response.data['results']), 0, "None instance could   be found")
        self.assertEqual(response.data['results'][0]['id'], instance.id)



    def test_post(self):
        # Test data
        test_data = self.get_test_post_data()

        # Creating with a POST request
        response = self.client.post(data=self.get_test_post_data(), format='json')

        # Testing success HTTP status
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Getting model by a provided pk
        instance = self.get_model().objects.get(pk=response.data['id'])

        # Comparing test data with model data
        instance_data = model_to_dict(instance)
        test_data['id'] = instance_data['id']
        self.assertEqual(test_data, instance_data)

    def test_patch(self):

        test_data = self.get_test_patch_before_data()

        # Creating new instance
        instance = self.get_model().objects.create(**test_data)

        # Another data to test after
        new_test_data = self.get_test_patch_after_data()

        # Partial update with a PATCH request
        response = self.client.patch(instance.id, data=new_test_data, format='json')

        # Testing success HTTP status
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Updating new data for comparison
        new_test_data.update({'id': instance.id})
        get_response = self.client.get(instance.id, format='json')

        self.assertEqual(get_response.data, new_test_data)

    def test_delete(self):

        test_data = self.get_test_delete_data()

        # Creating new room
        instance = self.get_model().objects.create(**test_data)

        # Checking creation
        instance.refresh_from_db()

        # Deleting with a DELETE request
        response = self.client.delete(instance.id, format='json')

        # Testing NO CONTENT HTTP status
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        # If model still exists, a fail will be raised. Otherwise, pass
        try:
            instance.refresh_from_db()
            self.fail("Instance from {0} still exists".format(str(self.get_model())))
        except self.get_model().DoesNotExist:
            pass

