#
# Dockerfile based on https://ruddra.com/2016/08/14/docker-django-nginx-postgres/
FROM python:3.5
ENV PYTHONUNBUFFERED 1
ADD /requirements.txt /
RUN pip install -r /requirements.txt
RUN mkdir /src;
RUN mkdir /src/logs -p;
RUN mkdir /src/static -p;
RUN mkdir /logs;
WORKDIR /src
